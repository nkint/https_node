var http = require('http');

module.exports.setup = function(app) {
    var p = 4001;

    var a = http.createServer(app);
    a.listen(p, function() {
        console.log('appA accessible only from appB %d', a.address().port);
    });
}