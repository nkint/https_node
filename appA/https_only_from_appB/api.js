var bodyParser = require('body-parser');

module.exports.setup = function(app){

    app.use(bodyParser());

    app.get('/api/secret', function(req, res) {
        var n = {message: 'secret'};
        res.json(n);
    });
}