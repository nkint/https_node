var http = require('http');

module.exports.setup = function(app) {
    var p = 4000;

    var a = http.createServer(app);
    a.listen(p, function() {
        console.log('appA accessible from everyone on port %d', a.address().port);
    });
}