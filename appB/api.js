var request = require('request');

module.exports.setup = function(app){
    app.get('/callAppA_public', function(req, res) {    
        var protocol = 'http://';
        var host = 'localhost';
        var port = ':4000'
        var uri = '/api/public/';

        var url = protocol+host+port+uri;
        request(url, function(err, response, body) {
            if(err) {
                console.log(err);
            } else {
                console.log('appB > callAppA_public > '+ err, response.statusCode, body);
                res.send(body)
            }
        }) 
    });

    app.get('/callAppA_secret', function(req, res) {    
        var protocol = 'http://';
        var host = 'localhost';
        var port = ':4001'
        var uri = '/api/secret/';

        var url = protocol+host+port+uri;
        request(url, function(err, response, body) {
            if(err) {
                console.log(err);
            } else {
                console.log('appB > callAppA_secret > '+ err, response.statusCode, body);
                res.send(body)
            }
        }) 
    });
}